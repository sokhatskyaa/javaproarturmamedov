import java.util.ArrayList;

public class Point {
    public static ArrayList<Integer> pointList = new ArrayList<>();

    public void addPointPlayer(int i) {
        pointList.add(i);
    }

    public void removePointPlayer(int i) {
        pointList.remove(i);
    }

    public void clearPointPlayer() {
        pointList.clear();
    }

    public int arrayPoint(int i) {
        return pointList.get(i);
    }
}
